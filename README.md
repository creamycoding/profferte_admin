# Admin panel

```sh
# local
lando db-import .local/db/dumps/database.sql

```

## login

- Username: admin@bootsback.com
- Password: 1234

### Sources:

- https://github.com/BootsBack/Codeigniter-php-Bootstrap-AdminLTE-Panel-with-user-management
