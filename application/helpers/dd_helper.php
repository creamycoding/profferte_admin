<?php

defined('BASEPATH') or exit('No direct script access allowed');

// Debug
if (!function_exists('dump')) {
    function dump(...$vars)
    {
        if (ON_DEV) {
            echo '<pre>';
            foreach ($vars as $var) {
                print_r($var);
                echo '<br>';
            }
            echo '</pre>';
        }
    }
}

if (!function_exists('dd')) {
    function dd(...$vars)
    {
        if (ON_DEV) {
            dump(...$vars);
            die();
        }
    }
}
