<?php

defined('BASEPATH') or exit('No direct script access allowed');


if (!function_exists('toCamelCase')) {
    /**
     * transform to camelCase
     *
     * @param [type] $string
     * @param boolean $capitalizeFirstCharacter
     * @return void
     */
    function toCamelCase($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }
}

if (!function_exists('getCallerFunctionName')) {
    /**
     *Get some previous function name
     * 
     * 1 is previous function; 
     * 0 is the current function where you would call this
     *  
     * @param integer $i 
     * @return void
     */
    function getCallerFunctionName($i = 1)
    {
        $dbt = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $i + 2);
        return $dbt[$i + 1]['function'] ?? null;
    }
}
