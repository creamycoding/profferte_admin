<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manage Forms</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Forms</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <?php if ($this->session->flashdata('success')) : ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif ($this->session->flashdata('error')) : ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <?php if (false && in_array('createForm', $user_permission)) : ?>
          <a href="<?php echo base_url('forms/create') ?>" class="btn btn-primary">Add Form</a>
          <br /> <br />
        <?php endif; ?>



        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage Forms</h3>


          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table id="userTable" class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Inserted</th>
                  <th>Domain</th>
                  <th>Before</th>
                  <th>Total quotes</th>

                  <?php if (in_array('updateForm', $user_permission) || in_array('deleteForm', $user_permission)) : ?>
                    <th>Action</th>
                  <?php endif; ?>
                </tr>
              </thead>
              <tbody>
                <?php if ($form_data) : ?>
                  <?php foreach ($form_data as $k => $v) :
                    $view = base_url('forms/view/' . $v['form_info']['form_id']); ?>
                    <tr>
                      <td><a href="<?php echo $view ?>"><?php echo $v['form_info']['form_id']; ?></a></td>
                      <td><?php echo $v['form_info']['form_inserted']; ?></td>
                      <td><?php echo $v['form_info']['form_domain'] ?></td>
                      <td><?php echo $v['form_info']['form_execution_date']; ?></td>
                      <td><?php echo count($v['form_quotes']) ?></td>

                      <?php if (in_array('viewForm', $user_permission) || in_array('updateForm', $user_permission) || in_array('deleteForm', $user_permission)) : ?>

                        <td>
                          <?php if (in_array('viewForm', $user_permission)) : ?>
                            <a href="<?php echo $view ?>" class="btn btn-default"><i class="fa fa-eye"></i></a>
                          <?php endif; ?>
                          <?php if (false && in_array('updateForm', $user_permission)) : ?>
                            <a href="<?php echo base_url('forms/edit/' . $v['form_info']['form_id']) ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                          <?php endif; ?>
                          <?php if (in_array('deleteForm', $user_permission)) : ?>
                            <a href="<?php echo base_url('forms/delete/' . $this->atri->en($v['form_info']['form_id'])) ?>" class="btn btn-default"><i class="fa fa-trash"></i></a>
                          <?php endif; ?>
                        </td>
                      <?php endif; ?>
                    </tr>
                  <?php endforeach ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
  $(document).ready(function() {
    $("#li-forms").addClass('menu-open');
    $("#link-forms").addClass('active');
    $("#manage-forms").addClass('active');
  });
</script>