<form role="form" action="<?php base_url('users/create') ?>" method="post">
    <div class="card-body">

        <?= validation_errors(); ?>

        <div class="form-group">
            <label for="execution_date">Execution date</label>
            <select class="form-control" id="execution_date" name="execution-date" style="width:100%">
                <option value="">Select Execution date</option>
                <?php foreach ($execution_data as $k => $v) : ?>
                    <option value="<?= $v['id'] ?>"><?= $v['value'] ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="form-group">
            <label for="groups">Groups</label>
            <select class="form-control" id="groups" name="groups" style="width:100%">
                <option value="">Select Groups</option>
                <?php foreach ($group_data as $k => $v) : ?>
                    <option value="<?= $v['id'] ?>" <?= ($user_group['id'] == $v['id']) ? 'selected'  : '' ?>><?= $v['group_name'] ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="form-group">
            <label for="groups">Store</label>
            <select class="form-control" id="store" name="store">
                <option value="">Select store</option>
                <?php foreach ($store_data as $k => $v) : ?>
                    <option value="<?= $v['id'] ?>" <?= ($user_data['store_id'] == $v['id']) ? "selected='selected'"  : '' ?>><?= $v['name'] ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?= $user_data['username'] ?? '' ?>" autocomplete="off">
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?= $user_data['email'] ?? '' ?>" autocomplete="off">
        </div>

        <div class="form-group">
            <label for="fname">First name</label>
            <input type="text" class="form-control" id="fname" name="fname" placeholder="First name" value="<?= $user_data['firstname'] ?? '' ?>" autocomplete="off">
        </div>

        <div class="form-group">
            <label for="lname">Last name</label>
            <input type="text" class="form-control" id="lname" name="lname" placeholder="Last name" value="<?= $user_data['lastname'] ?? '' ?>" autocomplete="off">
        </div>

        <div class="form-group">
            <label for="phone">Phone</label>
            <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?= $user_data['phone'] ?? '' ?>" autocomplete="off">
        </div>

        <div class="form-group">
            <label for="gender">Gender</label>
            <div class="radio">
                <label>
                    <input type="radio" name="gender" id="male" value="1" <?= (isset($user_data) && $user_data['gender'] == 1) ? 'checked' : '';  ?>>
                    Male
                </label>
                <label>
                    <input type="radio" name="gender" id="female" value="2" <?= (isset($user_data) && $user_data['gender'] == 2) ? 'checked' : '';  ?>>
                    Female
                </label>
            </div>
        </div>

        <div class="form-group">
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Leave the password field empty if you don't want to change.
            </div>
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" autocomplete="off">
        </div>

        <div class="form-group">
            <label for="cpassword">Confirm password</label>
            <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password" autocomplete="off">
        </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Save Changes</button>
        <a href="<?= base_url('users/') ?>" class="btn btn-warning">Back</a>
    </div>
</form>