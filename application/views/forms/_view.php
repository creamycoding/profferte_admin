<div class="card-body">
    <?php if (!empty($form_data['form_input'])) { ?>
        <table class="table table-bordered table-condensed table-hovered">
            <?php
            foreach ($form_data['form_input'] as $key => $value) { ?>
                <tr>
                    <th class="col-md-4 col-lg-2"><?= (empty(lang($key)) ? $key : lang($key)) ?></th>
                    <td><?= $value ?></td>
                </tr>

            <?php } ?>
        </table>
    <?php } else {
        echo 'No data found';
    } ?>
</div>