<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= ucfirst($action) ?> Forms</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Forms</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <?php if ($this->session->flashdata('success')) : ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php elseif ($this->session->flashdata('error')) : ?>
                    <div class="alert alert-error alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>

                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title"><?= ucfirst($action) ?> Form</h3>
                    </div>

                    <?php
                    switch ($action) {
                        case 'view':
                            $this->load->view('forms/_view');
                            break;

                        case 'edit':
                            $this->load->view('forms/_edit');
                            break;

                        case 'create':
                        default:
                            $this->load->view('forms/_create');
                            break;
                    } ?>

                </div>
                <!-- /.box -->
            </div>
            <!-- col-md-12 -->
        </div>
        <!-- /.row -->


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function() {
        const action = '<?= $action ?>';

        console.log(action);

        switch (action) {
            case 'view':

                break;
            case 'edit':
                $("#li-forms").addClass('menu-open');
                $("#link-forms").addClass('active');
                $("#manage-forms").addClass('active');

                break;
            case 'create':
            default:
                $("#li-forms").addClass('menu-open');
                $("#link-forms").addClass('active');
                $("#add-forms").addClass('active');
                break;
        }

    });
</script>

<?php
$p = '                                       ';
if (false && ON_DEV)
    dump($p . $action ?? $p . 'no action');

?>