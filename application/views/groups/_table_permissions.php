<div class=" table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th></th>
                <th>Create</th>
                <th>Update</th>
                <th>View</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 0;
            foreach ($permissions as $perm) {
                echo '<tr>';
                if (isset($perm['title'])) { ?>
                    <td><?= $perm['title'] ?></td>
                    <?php }
                foreach ($perm['crud'] as $crud_key => $crud) {
                    if ($crud) {
                        $value = $crud_key . ucfirst($perm['name']);

                        if (isset($serialize_permission)) {
                            // on edit
                            $checked = $serialize_permission && in_array($value, $serialize_permission) ? 'checked' : ''; ?>
                            <td>
                                <input type="checkbox" name="permission[]" id="permission" value="<?= $value ?>" <?= $checked ?>>
                            </td>
                        <?php } else {
                            // on create 
                        ?>
                            <td>
                                <div class="custom-control custom-switch"><input type="checkbox" name="permission[]" class="custom-control-input" id="customSwitch<?= ++$i ?>" value="<?= $value ?>"><label class="custom-control-label" for="customSwitch<?= $i ?>">&nbsp;</label></div>
                            </td>
                        <?php
                        } ?>


            <?php } else {
                        echo '<td> - </td>';
                    }
                }
                echo '</tr>';
            } ?>

        </tbody>
    </table>
</div>