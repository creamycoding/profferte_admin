<?php

class Model_forms extends MY_model
{
	public $tb_name = 'forms';
	public $p = 'form_';

	public function __construct()
	{
		parent::__construct();
	}


	public function getFormData($formId = null)
	{
		$data = MY_model::getData($formId);
		if (!empty($data['form_input'])) $data['form_input'] = json_decode($data['form_input']);
		return $data;
	}

	public function create($data = '')
	{
		if (ON_DEV && empty($data)) {
			$data = $this->debugData();
		}

		$data = $this->getValidData($data, ['inserted', 'input', 'execution_date', 'domain']);
		if ($data == false)
			return false;

		$create = $this->db->insert($this->tb_name, $data);
		return ($create == true) ? true : false;
	}

	public function edit($data, $id)
	{
		$this->db->where('id', $id);
		$update = $this->db->update($this->tb_name, $data);
		return ($update == true) ? true : false;
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$delete = $this->db->delete($this->tb_name);
		return ($delete == true) ? true : false;
	}

	private function getValidData($values, $add = null)
	{
		$p = $this->p;
		$valid = ['id', 'inserted', 'deleted', 'domain', 'project', 'execution_date', 'input'];
		$valid = preg_filter('/^/', $p, $valid);

		if (isset($add)) {
			if (in_array('inserted', $add)) $values[$p . 'inserted'] = date('Y-m-d H:i:s');
			if (in_array('input', $add)) $values[$p . 'input'] = json_encode($values);
			if (in_array('domain', $add)) $values[$p . 'domain'] = $this->config->item('base_url');
			if ((in_array('execution_date', $add))) {
				$input_execution_date = $values[$p . 'execution-date'] ?? '';
				$values[$p . 'execution_date'] = $this->getExecutionDate($input_execution_date);
			}
		}

		foreach ($values as $key => $value) {
			if (!in_array($key, $valid))
				unset($values[$key]);
		}

		return $values;
	}

	private function getExecutionDate(String $string)
	{
		switch ($string) {
			case 'Binnen 1 maand':
			case 'Within 1 month':
				$time = '+1 month';
				break;
			case 'Binnen 3 maanden':
			case 'Within 3 months':
				$time = '+3 months';
				break;
			case 'Binnen 6 maanden':
			case 'Within 6 months':
				$time = '+6 months';
				break;
			case 'Binnen het jaar':
			case 'Within the year':
				$time = '+1 year';
				break;
			case 'Graag zo snel mogelijk':
			case 'As quickly as possible':
			default:
				# asap
				break;
		}

		return date('Y-m-d', strtotime($time ?? ''));
	}

	public function getExecutionData($translate = true)
	{
		$data = [
			'EXECUTION_DATE_ASAP',
			'EXECUTION_DATE_1_MONTH',
			'EXECUTION_DATE_3_MONTH',
			'EXECUTION_DATE_6_MONTH',
			'EXECUTION_DATE_1_YEAR'
		];

		if ($translate) array_walk($data, function (&$var) {
			$var = [
				'id' => $var,
				'value' => lang($var),
			];
		});

		return $data;
	}

	private function debugData()
	{
		return [
			$this->p . 'form-project' => 'Elektriciteitswerken',
			$this->p . 'type-building' => 'Appartement',
			$this->p . 'type-work' => 'Bekabeling',
			$this->p . 'execution-date' => 'Binnen 6 maanden',
			$this->p . 'form-description' => 'Wat moet er gebeuren?',
			$this->p . 'form-street' => 'Straat',
			$this->p . 'form-postal' => 'Postcode',
			$this->p . 'form-place' => 'Gemeente',
			$this->p . 'form-firstname' => 'Voornaam',
			$this->p . 'form-lastname' => 'Achternaam',
			$this->p . 'form-phone' => '0494123456',
			$this->p . 'form-email' => 'jan.modaal@gmail.com',
		];
	}
}
