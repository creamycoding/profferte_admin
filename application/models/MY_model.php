<?php

class MY_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        // $this->id = ($this->p ?? '') . 'id';
        $this->id = isset($this->p) ? $this->p  . 'id' : 'id';
        // $this->id = 'form_id';
    }

    public function getData($id = null)
    {
        if ($id) {
            $sql = "SELECT * FROM $this->tb_name WHERE $this->id = ?";
            $query = $this->db->query($sql, array($id));
            return $query->row_array();
        }

        $sql = "SELECT * FROM $this->tb_name ORDER BY $this->id DESC";

        $query = $this->db->query($sql, array(1));
        return $query->result_array();
    }

    public function create($data = '')
    {
        $create = $this->db->insert($this->tb_name, $data);
        return ($create == true) ? true : false;
    }

    public function edit($data, $id)
    {
        $this->db->where($this->id, $id);
        $update = $this->db->update($this->tb_name, $data);
        return ($update == true) ? true : false;
    }

    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $delete = $this->db->delete($this->tb_name);
        return ($delete == true) ? true : false;
    }
}
