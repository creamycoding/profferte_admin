<?php

class Model_quotes extends MY_model
{
	public $tb_name = 'quotes';
	public $p = 'quot_';

	public function __construct()
	{
		parent::__construct();
	}


	public function getFormData($quoteId = null)
	{
		return MY_model::getData($quoteId);
	}

	public function create($data = '')
	{
		if (ON_DEV && empty($data)) {
			$data = $this->debugData();
		}



		$data = $this->getValidData($data, ['inserted']);
		if ($data == false) return false;

		// dd($data);
		$create = $this->db->insert($this->tb_name, $data);
		return ($create == true) ? true : false;
	}

	public function edit($data, $id)
	{
		return MY_model::getData($data, $id);

		// $this->db->where('id', $id);
		// $update = $this->db->update($this->tb_name, $data);
		// return ($update == true) ? true : false;
	}

	public function delete($id)
	{
		return MY_model::delete($id);

		// $this->db->where('id', $id);
		// $delete = $this->db->delete($this->tb_name);
		// return ($delete == true) ? true : false;
	}

	public function getFormQuotes($formId)
	{
		if ($formId) {
			$this->db->where('quot_form', $formId);
			$query = $this->db->get('quotes');
			return $query->result();
		}
	}

	private function getValidData($values, $add = null)
	{
		$p = $this->p;
		$valid = ['id', 'store', 'form', 'inserted', 'deleted', 'date_expire', 'price'];
		$valid = preg_filter('/^/', $p, $valid);

		if (isset($add)) {
			if (in_array('inserted', $add)) $values[$p . 'inserted'] = date('Y-m-d H:i:s');
		}

		foreach ($values as $key => $value) {
			if (!in_array($key, $valid))
				unset($values[$key]);
		}

		return $values;
	}


	private function debugData()
	{
		return [
			$this->p . 'store' => '1',
			$this->p . 'form' => '1',
			$this->p . 'date_expire' => date('Y-m-d', strtotime('+1 month')),
			$this->p . 'price' => rand(0, 2000) . '.' . str_pad(rand(0, 99), 2, '0', STR_PAD_LEFT),
		];
	}
}
