<?php

class Model_permissions extends CI_Model
{
	public $tb_name = 'permissions';

	public function __construct()
	{
		parent::__construct();
	}

	public function getPermissionData($permissionId = null)
	{
		$permissions = $this->permissionData();

		if ($permissionId) {
			$key = array_search($permissionId, array_column($permissions, 'id'));
			return $permissions[$key];
		}

		return $permissions;
	}

	private function permissionData()
	{
		return [
			[
				'id' => '1',
				'name' => 'user',
				'title' => 'Users',
				'crud' => [
					'create' => true,
					'update' => true,
					'view' => true,
					'delete' => true,
				]
			],
			[
				'id' => '2',
				'name' => 'group',
				'title' => 'Groups',
				'crud' => [
					'create' => true,
					'update' => true,
					'view' => true,
					'delete' => true,
				]
			],
			[
				'id' => '3',
				'name' => 'store',
				'title' => 'Stores',
				'crud' => [
					'create' => true,
					'update' => true,
					'view' => true,
					'delete' => true,
				]
			],
			[
				'id' => '4',
				'name' => 'company',
				'title' => 'Company',
				'crud' => [
					'create' => false,
					'update' => true,
					'view' => false,
					'delete' => false,
				]
			],
			[
				'id' => '5',
				'name' => 'profile',
				'title' => 'Profile',
				'crud' => [
					'create' => false,
					'update' => true,
					'view' => true,
					'delete' => false,
				],
			],
			[
				'id' => '6',
				'name' => 'setting',
				'title' => 'Settings',
				'crud' => [
					'create' => false,
					'update' => true,
					'view' => true,
					'delete' => false,
				],
			],
			[
				'id' => '7',
				'name' => 'form',
				'title' => 'Forms',
				'crud' => [
					'create' => true,
					'update' => true,
					'view' => true,
					'delete' => true,
				],
			],
		];
	}
}
