<?php

class Forms extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Forms';


		$this->load->model('model_users');
		$this->load->model('model_groups');
		$this->load->model('model_stores');

		$this->load->model('model_forms');
		$this->load->model('model_quotes');
	}
	public function index()
	{
		redirect('forms/manage', 'refresh');
	}
	public function manage()
	{

		if (!in_array('viewForm', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$form_data = $this->model_forms->getFormData();

		$result = [];
		foreach ($form_data as $k => $v) {
			// form
			$v['form_input'] = json_decode($v['form_input']);
			$result[$k]['form_info'] = $v;
			// quotes
			$quotes = $this->model_quotes->getFormQuotes($v['form_id']) ?? [];
			$result[$k]['form_quotes'] = $quotes;
		}

		$this->data['form_data'] = $result;

		$this->render_template('forms/index', $this->data);
	}

	public function create()
	{

		if (!in_array('createForm', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$this->form_validation->set_rules('groups', 'Group', 'required');
		$this->form_validation->set_rules('store', 'Store', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]|is_unique[users.username]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('cpassword', 'Confirm password', 'trim|required|matches[password]');
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');


		if ($this->form_validation->run() == TRUE) {
			// true case
			$password = $this->password_hash($this->input->post('password'));
			$data = array(
				'username' => $this->input->post('username'),
				'password' => $password,
				'email' => $this->input->post('email'),
				'firstname' => $this->input->post('fname'),
				'lastname' => $this->input->post('lname'),
				'phone' => $this->input->post('phone'),
				'gender' => $this->input->post('gender'),
				'store_id' => $this->input->post('store'),
			);

			$create = $this->model_users->create($data, $this->input->post('groups'));
			if ($create == true) {
				$this->session->set_flashdata('success', 'Successfully created');
				redirect('forms/', 'refresh');
			} else {
				$this->session->set_flashdata('errors', 'Error occurred!!');
				$this->getData();
				redirect('forms/save', 'refresh');
			}
		} else {
			// false case
			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				$this->session->set_flashdata('errors', 'Error occurred!!');
			}

			$this->getData();
			$this->render_template('forms/save', $this->data);
		}
	}

	public function password_hash($pass = '')
	{
		if ($pass) {
			$password = password_hash($pass, PASSWORD_DEFAULT);
			return $password;
		}
	}

	public function view($id = null)
	{
		if (!in_array('viewForm', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		$this->getData($id);
		// dd($this->data);
		$this->render_template('forms/save', $this->data);
	}

	public function edit($id = null)
	{

		if (!in_array('updateForm', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		if ($id) {
			$this->form_validation->set_rules('groups', 'Group', 'required');
			$this->form_validation->set_rules('store', 'Store', 'trim|required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('fname', 'First name', 'trim|required');


			if ($this->form_validation->run() == TRUE) {
				// true case
				if (empty($this->input->post('password')) && empty($this->input->post('cpassword'))) {
					$data = array(
						'username' => $this->input->post('username'),
						'email' => $this->input->post('email'),
						'firstname' => $this->input->post('fname'),
						'lastname' => $this->input->post('lname'),
						'phone' => $this->input->post('phone'),
						'gender' => $this->input->post('gender'),
						'store_id' => $this->input->post('store'),
					);

					$update = $this->model_users->edit($data, $id, $this->input->post('groups'));
					if ($update == true) {
						$this->session->set_flashdata('success', 'Successfully created');
						redirect('forms/', 'refresh');
					} else {
						$this->session->set_flashdata('errors', 'Error occurred!!');
						redirect('forms/save/' . $id, 'refresh');
					}
				} else {
					//$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
					//$this->form_validation->set_rules('cpassword', 'Confirm password', 'trim|required|matches[password]');

					if ($this->form_validation->run() == TRUE) {

						$password = $this->password_hash($this->input->post('password'));

						$data = array(
							'username' => $this->input->post('username'),
							'password' => $password,
							'email' => $this->input->post('email'),
							'firstname' => $this->input->post('fname'),
							'lastname' => $this->input->post('lname'),
							'phone' => $this->input->post('phone'),
							'gender' => $this->input->post('gender'),
							'store_id' => $this->input->post('store'),
						);

						$update = $this->model_users->edit($data, $id, $this->input->post('groups'));
						if ($update == true) {
							$this->session->set_flashdata('success', 'Successfully updated');
							redirect('forms/', 'refresh');
						} else {
							$this->session->set_flashdata('errors', 'Error occurred!!');
							redirect('forms/save/' . $id, 'refresh');
						}
					} else {
						// false case
						$this->getData($id);
						$this->render_template('forms/save', $this->data);
					}
				}
			} else {
				// false case
				$this->getData($id);
				$this->render_template('forms/save', $this->data);
			}
		}
	}

	public function delete($id)
	{

		if (!in_array('deleteForm', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		if ($id) {
			if ($this->input->post('confirm')) {


				$delete = $this->model_users->delete($this->atri->de($id));
				if ($delete == true) {
					$this->session->set_flashdata('success', 'Successfully removed');
					redirect('forms/', 'refresh');
				} else {
					$this->session->set_flashdata('error', 'Error occurred!!');
					redirect('forms/delete/' . $id, 'refresh');
				}
			} else {
				$this->data['id'] = $id;
				$this->render_template('forms/delete', $this->data);
			}
		}
	}

	private function getData($id = null)
	{
		$this->data['store_data'] = $this->model_stores->getStoresData();

		if ($id) {
			$this->data['group_data'] = $this->model_groups->getGroupData();
			$this->data['user_data'] = $this->model_users->getUserData($id);
			$this->data['form_data'] = $this->model_forms->getFormData($id);
			$this->data['user_group'] = $this->model_users->getUserGroup($id);
		}

		$this->data['action'] = getCallerFunctionName();
		// dd($this->data);
	}

	public function profile()
	{

		if (!in_array('viewProfile', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$user_id = $this->session->userdata('id');

		$user_data = $this->model_users->getUserData($user_id);
		$this->data['user_data'] = $user_data;

		$user_group = $this->model_users->getUserGroup($user_id);
		$this->data['user_group'] = $user_group;

		$this->render_template('forms/profile', $this->data);
	}

	public function setting()
	{
		if (!in_array('updateSetting', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$id = $this->session->userdata('id');

		if ($id) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('fname', 'First name', 'trim|required');


			if ($this->form_validation->run() == TRUE) {
				// true case
				if (empty($this->input->post('password')) && empty($this->input->post('cpassword'))) {
					$data = array(
						'username' => $this->input->post('username'),
						'email' => $this->input->post('email'),
						'firstname' => $this->input->post('fname'),
						'lastname' => $this->input->post('lname'),
						'phone' => $this->input->post('phone'),
						'gender' => $this->input->post('gender'),
					);

					$update = $this->model_users->edit($data, $id, $this->input->post('groups'));
					if ($update == true) {
						$this->session->set_flashdata('success', 'Successfully updated');
						redirect('forms/setting/', 'refresh');
					} else {
						$this->session->set_flashdata('errors', 'Error occurred!!');
						redirect('forms/setting/', 'refresh');
					}
				} else {
					//$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
					//$this->form_validation->set_rules('cpassword', 'Confirm password', 'trim|required|matches[password]');

					if ($this->form_validation->run() == TRUE) {

						$password = $this->password_hash($this->input->post('password'));

						$data = array(
							'username' => $this->input->post('username'),
							'password' => $password,
							'email' => $this->input->post('email'),
							'firstname' => $this->input->post('fname'),
							'lastname' => $this->input->post('lname'),
							'phone' => $this->input->post('phone'),
							'gender' => $this->input->post('gender'),
						);

						$update = $this->model_users->edit($data, $id, $this->input->post('groups'));
						if ($update == true) {
							$this->session->set_flashdata('success', 'Successfully updated');
							redirect('forms/setting/', 'refresh');
						} else {
							$this->session->set_flashdata('errors', 'Error occurred!!');
							redirect('forms/setting/', 'refresh');
						}
					} else {
						// false case
						$user_data = $this->model_users->getUserData($id);
						$groups = $this->model_users->getUserGroup($id);

						$this->data['user_data'] = $user_data;
						$this->data['user_group'] = $groups;

						$group_data = $this->model_groups->getGroupData();
						$this->data['group_data'] = $group_data;

						$this->render_template('forms/setting', $this->data);
					}
				}
			} else {
				// false case
				$user_data = $this->model_users->getUserData($id);
				$groups = $this->model_users->getUserGroup($id);

				$this->data['user_data'] = $user_data;
				$this->data['user_group'] = $groups;

				$group_data = $this->model_groups->getGroupData();
				$this->data['group_data'] = $group_data;

				$this->render_template('forms/setting', $this->data);
			}
		}
	}
}
