<?php
class LangSwitch extends CI_Controller
{

    public $controller = 'lang_switch';
    public function __construct()
    {
        parent::__construct();
        $this->controller = toCamelCase($this->controller);
        $this->load->helper('url');
    }

    function switchLanguage($language = "")
    {
        // get settings
        global $CFG, $IN;
        $config = &$CFG->config;

        // new language, or default
        $language = ($language != "") ? $language : $config['language'];

        // name of the cookie/session
        $name = 'user_lang';

        $IN->set_cookie($name, $language, $config['sess_expiration']);
        $this->session->set_userdata($name, $language);

        redirect(base_url() . $language);
    }
}
