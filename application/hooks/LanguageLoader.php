<?php

/**
 * Created by PhpStorm.
 * User: huzkaymak
 * Date: 7/09/17
 * Time: 09:57
 */
class LanguageLoader
{
    function initialize()
    {
        $ci = &get_instance();
        $ci->load->helper('language');

        global $URI, $CFG, $IN;
        $config = &$CFG->config;
        $lang_abbr = $IN->cookie($config['cookie_prefix'] . 'user_lang');

        $first_segment = $URI->segment(1);
        if (isset($config['lang_uri_abbr'][$first_segment]))
            if (!isset($lang_abbr) || ($first_segment != $lang_abbr))
                $lang_abbr = $first_segment;

        $language = $config['lang_uri_abbr'][$lang_abbr] ?? $config['language'];


        $ci->lang->load('common', 'dutch');
        if ($language) {
            $ci->lang->load('global', $language);

            // load if exist
            if (!empty($ci->controller) && $this->lang_file_exist($ci->controller, $language))
                $ci->lang->load($ci->controller, $language);
        } else {
            $ci->lang->load('global', $this->config->config['language']);
        }
    }

    private function lang_file_exist($controller, $language)
    {
        $file = FCPATH . 'application/language/' . $language . '/' . $controller . '_lang.php';
        dd($file);
        return file_exists($file);
    }
}
