<?php
$lang["LANG_CODE"] = "nl";

// Execution date's
$lang['EXECUTION_DATE_ASAP'] = 'Graag zo snel mogelijk';
$lang['EXECUTION_DATE_1_MONTH'] = 'Binnen 1 maand';
$lang['EXECUTION_DATE_3_MONTH'] = 'Binnen 3 maanden';
$lang['EXECUTION_DATE_6_MONTH'] = 'Binnen 6 maanden';
$lang['EXECUTION_DATE_1_YEAR'] = 'Binnen het jaar';
