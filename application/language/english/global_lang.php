<?php
$lang["LANG_CODE"] = "en";

// Execution date's
$lang['EXECUTION_DATE_ASAP'] = 'As quickly as possible';
$lang['EXECUTION_DATE_1_MONTH'] = 'Within 1 month';
$lang['EXECUTION_DATE_3_MONTH'] = 'Within 6 months';
$lang['EXECUTION_DATE_6_MONTH'] = 'Within 6 months';
$lang['EXECUTION_DATE_1_YEAR'] = 'Within the year';
