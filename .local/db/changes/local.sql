/* add forms */
CREATE TABLE `forms` (
    `form_id` INT(11) NOT NULL AUTO_INCREMENT,
    `form_inserted` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `form_deleted` DATETIME NULL,
    `form_domain` VARCHAR(255) NULL,
    `form_project` INT NULL,
    `form_execution_date` DATE NOT NULL,
    `form_input` JSON NOT NULL,
    PRIMARY KEY (`form_id`)
) ENGINE = InnoDB;


/* forms linked to a quote */
CREATE TABLE `lamp`.`quotes` (
    `quot_id` INT(11) NOT NULL AUTO_INCREMENT,
    `quot_store` INT(11) NOT NULL,
    `quot_form` INT(11) NOT NULL,
    `quot_date_expire` DATE NULL,
    `quot_price` DECIMAL NOT NULL,
    `quot_inserted` DATETIME NOT NULL,
    `quot_deleted` DATETIME NOT NULL,
    PRIMARY KEY (`quot_id`)
) ENGINE = InnoDB;